#!/bin/bash

#テスト用DB作成
mysql -u root -e "CREATE DATABASE IF NOT EXISTS TEST_DB"

#テスト用DB取り込み
mysql -u root --comments TEST_DB < /db/TEST_DB.dump

#ユーザ作成
mysql -u root -e "GRANT ALL ON TEST_DB.* TO test_db_user@'localhost' identified by 'test_db_pass'"
mysql -u root -e "GRANT ALL ON TEST_DB.* TO test_db_user@'%' identified by 'test_db_pass'"
