<?php
//$classloader = new ClassLoader();
//
////オートロード対象ディレクトリ登録
//$classloader->registerDirs(realpath(dirname(__FILE__).'/src/'));
//
////オートロード実行
//$classloader->register();
//
///**
// * オートローダークラス
// * Class ClassLoader
// */
//class ClassLoader
//{
//    protected $dirs = array();
//    protected $readFileCache = array();
//
//    public function register(){
//        spl_autoload_register(array($this, 'loadClass'));
//    }
//
//    /**
//     * オートロード対象ディレクトリの登録
//     * @param $dir
//     */
//    public function registerDirs($dir)
//    {
//        $this->dirs[] = $dir;
//
//    }
//
//    public function loadClass($class)
//    {
//        if (isset($this->readFileCache[$class.'.php'])) {
//            require_once $this->readFileCache[$class.'.php'];
//            return;
//        }
//        foreach ($this->dirs as $dir) {
//            $bool = $this->recursiveSearchClass($dir,$class);
//            if ($bool) {
//                return;
//            }
//        }
//
//    }
//
//    private function recursiveSearchClass($dir, $class)
//    {
//        foreach (array_diff(scandir($dir), array('.','..')) as $file) {
//
//            if (is_file($dir.'/'.$file) && is_readable($dir.'/'.$file)) {
//                $this->readFileCache[$file] = $dir.'/'.$file;
////                print_r($dir.'/'.$file."\n");
//                if ($file == $class.'.php'){
//                    require_once $dir.'/'.$class.'.php';
//                    return true;
//                }
//            }
//            if (is_dir($dir.'/'.$file)) {
//                $bool = $this->recursiveSearchClass($dir.'/'.$file, $class);
//                if ($bool) {
//                    return true;
//                }
//            }
//        }
//        return false;
////        exit;
//    }
//
//
//}