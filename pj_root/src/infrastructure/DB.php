<?php
namespace php_unit_sample_pj\infrastructure;

class DB
{

    public static $DSN;
    public static $DB_USER;
    public static $DB_PASSWD;

    public static function setDB($DSN,$DB_USER,$DB_PASSWD){
        self::$DSN = $DSN;
        self::$DB_USER = $DB_USER;
        self::$DB_PASSWD = $DB_PASSWD;

    }

    public static function getDB(){
        try{
            $pdo = new \PDO(self::$DSN,self::$DB_USER,self::$DB_PASSWD);
            return $pdo;

        } catch (\PDOException $e) {
            return null;
        }

    }

}