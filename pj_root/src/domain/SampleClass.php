<?php
namespace php_unit_sample_pj\domain;

class SampleClass
{

    private function increment($number){
        $number++;
        return $number;
    }

    private function getTestArray(){
        return [
            'keyA'=>'valueA',
            'keyB'=>'valueB',
            'keyC'=>'valueC',
        ];
    }
}