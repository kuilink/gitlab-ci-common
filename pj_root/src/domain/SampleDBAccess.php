<?php
namespace php_unit_sample_pj\domain;

use php_unit_sample_pj\infrastructure\DB;

class SampleDBAccess
{
    /**
     * TEST_TABLEにレコードを追加するメソッド
     * @param $id（オートインクリメント）
     * @param $name
     * @return bool
     */
    private function addId($id,$name)
    {
        $pdo = DB::getDB();
        $stmt = $pdo->prepare('insert into TEST_TABLE (id, name) values(?,?)');
        $stmt->bindValue(1, $id,\PDO::PARAM_INT);
        $stmt->bindValue(2, $name,\PDO::PARAM_STR);
        return $stmt->execute();
    }
}