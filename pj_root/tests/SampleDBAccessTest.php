<?php
namespace php_unit_sample_pj\domain;
require_once __DIR__.'/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\YamlDataSet;
//use com\kouji\SampleDBAccess;
use php_unit_sample_pj\infrastructure\DB;
use php_unit_sample_pj\domain\SampleDBAccess;
// require_once dirname(__FILE__)."/../ClassLoader.php";


class SampleDBAccessTest extends TestCase
{
    use TestCaseTrait;

    // PDO のインスタンス生成は、クリーンアップおよびフィクスチャ読み込みのときに一度だけ
    static private $pdo = null;

    // PHPUnit\DbUnit\Database\Connection のインスタンス生成は、テストごとに一度だけ
    private $conn = null;

    public function getConnection()
    {

        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO( $GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD'] );
            }


            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    /**
     * @test
     */
    public function addIDテスト()
    {
        $this->setTestDb();

        $instance = new SampleDBAccess();

        //ReflectionMethod を作成（クラス名::メソッド名）
        $addId = new \ReflectionMethod( SampleDBAccess::class,'addId' );

        //privateメソッドをアクセス可能にする
        $addId->setAccessible(true);

        for( $i = 111; $i <= 200; $i++ ){
            //ReflectionMethod を実行する
            $argument = array( null, 'name' . $i );
            $addId->invokeArgs($instance,$argument);
        }

        $result = DB::getDB()->query("select name from TEST_TABLE where id = 50" );
        $result = $result->fetch(\PDO::FETCH_ASSOC);

        $this->assertEquals( 'name150' , $result["name"]);
//        exit;
    }


   /**
     * @test
     */
    public function invalidなDB()
    {
        $this->setInvalidDb();
        $this->assertNull(DB::getDB());
    }


    /**
     * @return PHPUnit\Extensions\Database\DataSet\IDataSet
     */
    public function getDataSet()
    {
//        return $this->createXMLDataSet(dirname(__FILE__)."/id.xml");
         return new YamlDataSet(dirname(__FILE__)."/id.yaml");
    }

    private function setTestDb(){
        DB::setDB($GLOBALS['DB_DSN'],$GLOBALS['DB_USER'],$GLOBALS['DB_PASSWD']);
    }

    private function setInvalidDb(){
        DB::setDB("aaa","aaa","aaa");
    }
}