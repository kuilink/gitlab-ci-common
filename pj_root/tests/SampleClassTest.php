<?php
require_once __DIR__.'/../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use php_unit_sample_pj\domain\SampleClass;
use php_unit_sample_pj\ValueObject\SampleValueObject;

class SampleClassTest extends TestCase
{
    /**
     * @test
     * @dataProvider SampleClass_incrementテスト_テストパラメーター
     * @param $argument
     * @param $expected
     * @throws ReflectionException
     */
    public function SampleClass_incrementテスト($argument, $expected)
    {
        //ReflectionMethod を作成（クラス名::メソッド名）
        $method_increment = new ReflectionMethod(SampleClass::class, 'increment' );

        //privateメソッドをアクセス可能にする
        $method_increment->setAccessible(true);

        //ReflectionMethod を実行する
        $actual = $method_increment->invoke( new SampleClass(), $argument);

        $this->assertSame($expected , $actual);
    }

    public function SampleClass_incrementテスト_テストパラメーター(){
        return [
            "1つめ" => [1, 2],
            "2つめ" => [2, 3],
        ];
    }


        /**
     * @test
     * @dataProvider getArrayテスト_テストパラメーター
     * @param $argument
     * @param $expected
     * @throws ReflectionException
     */
    public function assertArrayHasKey_getArrayテスト($argument)
    {

        //クラスインスタンスを作る
        $instance = new SampleClass();

        //ReflectionMethod を作成（クラス名::メソッド名）
        $method_increment = new ReflectionMethod('php_unit_sample_pj\domain\SampleClass::getTestArray');

        //privateメソッドをアクセス可能にする
        $method_increment->setAccessible(true);

        //ReflectionMethod を実行する
        $array = $method_increment->invoke($instance);


        $this->assertArrayHasKey($argument , $array);

    }

    public function  getArrayテスト_テストパラメーター(){
        return [
            ['keyA'],
            ['keyB'],
            ['keyC'],
        ];

    }


    /**
     * @test
     * @dataProvider assertClassHasAttributeテスト_テストパラメーター
     * @param $argument
     * @param $expected
     * @throws ReflectionException
     */
    public function assertClassHasAttributeテスト($attribute_name)
    {

        //クラスインスタンスを作る

        $this->assertClassHasAttribute($attribute_name , SampleValueObject::class);

    }

    public function assertClassHasAttributeテスト_テストパラメーター(){
        return [
            ['hoge'],
            ['fuga'],
            ['moge'],
        ];

    }
}