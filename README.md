# PHPUnitサンプル
- docker 環境でPHPUnitを動かすサンプルです
- DBUnitも含みます
- GitLabCIを動かします

##

### docker-compose
```
$ docker -v
Docker version 19.03.8, build afacb8b

$ docker-compose -v
docker-compose version 1.26.0, build d4451659

$ docker-compose up -d
Creating network "sample_test_default" with the default driver
Creating sample_test_database_1 ... done
Creating sample_test_composer_1 ... done
```
### composerコンテナに入る
```
$ docker exec -it sample_test_composer_1 /bin/bash
bash-5.0#
```
### composerコンテナ内でphpunitの実行
```
bash-5.0# vendor/bin/phpunit  --coverage-text --colors=always --stop-on-failure --debug
PHPUnit 8.5.8 by Sebastian Bergmann and contributors.

Test 'SampleClassTest::SampleClass_incrementテスト with data set "1つめ" (1, 2)' started
Test 'SampleClassTest::SampleClass_incrementテスト with data set "1つめ" (1, 2)' ended
Test 'SampleClassTest::SampleClass_incrementテスト with data set "2つめ" (2, 3)' started
Test 'SampleClassTest::SampleClass_incrementテスト with data set "2つめ" (2, 3)' ended
Test 'domain\SampleDBAccessTest::addIDテスト' started
Test 'domain\SampleDBAccessTest::addIDテスト' ended
Test 'domain\SampleDBAccessTest::invalidなDB' started
Test 'domain\SampleDBAccessTest::invalidなDB' ended


Time: 465 ms, Memory: 6.00 MB

OK (4 tests, 4 assertions)

Generating code coverage report in Clover XML format ... done [51 ms]

Generating code coverage report in HTML format ... done [507 ms]


Code Coverage Report:
  2020-07-21 00:28:39

 Summary:
  Classes: 100.00% (3/3)
  Methods: 100.00% (4/4)
  Lines:   100.00% (16/16)

\domain::domain\SampleClass
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  2/  2)
\domain::domain\SampleDBAccess
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  6/  6)
\infrastructure::infrastructure\DB
  Methods: 100.00% ( 2/ 2)   Lines: 100.00% (  8/  8)
```
### GitLabCI
gitlabにpushすることで動きます
#### push
```
git push origin master
```
#### GitLabCI稼働サンプル
```
Running with gitlab-runner 13.2.0-rc2 (45f2b4ec)
  on docker-auto-scale fa6cab46
Preparing the "docker+machine" executor
00:44
Using Docker executor with image composer:latest ...
Starting service mysql:5.6 ...
Pulling docker image mysql:5.6 ...
Using docker image sha256:01ba04febd24365f377e0e5d8c7d5612520bffb1f3fded337dfbf46fbf6fd3b6 for mysql:5.6 ...
WARNING: Service mysql:5.6 is already created. Ignoring.
Waiting for services to be up and running...
Pulling docker image composer:latest ...
Using docker image sha256:36bf7259fa4563a6aa12b905389d75598843bff3cdc130a1387b65e531ff2726 for composer:latest ...
Preparing environment
00:03
Running on runner-fa6cab46-project-19660282-concurrent-0 via runner-fa6cab46-srm-1595255252-df3d5c79...
Getting source from Git repository
・・・
・・・
・・・
PHPUnit 8.5.8 by Sebastian Bergmann and contributors.
Test 'SampleClassTest::SampleClass_incrementテスト with data set "1つめ" (1, 2)' started
Test 'SampleClassTest::SampleClass_incrementテスト with data set "1つめ" (1, 2)' ended
Test 'SampleClassTest::SampleClass_incrementテスト with data set "2つめ" (2, 3)' started
Test 'SampleClassTest::SampleClass_incrementテスト with data set "2つめ" (2, 3)' ended
Test 'domain\SampleDBAccessTest::addIDテスト' started
Test 'domain\SampleDBAccessTest::addIDテスト' ended
Test 'domain\SampleDBAccessTest::invalidなDB' started
Test 'domain\SampleDBAccessTest::invalidなDB' ended
Time: 257 ms, Memory: 6.00 MB
OK (4 tests, 4 assertions)
Generating code coverage report in Clover XML format ... done [7 ms]
Generating code coverage report in HTML format ... done [11 ms]
Code Coverage Report:     
  2020-07-20 14:29:59     
                          
 Summary:                 
  Classes: 100.00% (3/3)  
  Methods: 100.00% (4/4)  
  Lines:   100.00% (16/16)
\domain::domain\SampleClass
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  2/  2)
\domain::domain\SampleDBAccess
  Methods: 100.00% ( 1/ 1)   Lines: 100.00% (  6/  6)
\infrastructure::infrastructure\DB
  Methods: 100.00% ( 2/ 2)   Lines: 100.00% (  8/  8)
Uploading artifacts for successful job
00:04
Uploading artifacts...
coverage/: found 30 matching files and directories 
Uploading artifacts as "archive" to coordinator... ok  id=647010396 responseStatus=201 Created token=b4sAszow
Job succeeded
```

#### namespace追加時の作業
```php
bash-5.0# composer dump-autoload
Generating autoload files
Generated autoload files containing 659 classes
```